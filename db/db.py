import logging

import psycopg2 as psycopg2
from psycopg2.extras import RealDictCursor

from config import *

logging.basicConfig(level=LOG_LEVEL)


class PostgresDb:
    UPDATE_SQL = 'UPDATE {table} SET {update_fields} WHERE {conditions};'
    INSERT_SQL = 'INSERT INTO {table} ({columns}) VALUES ({values});'
    SELECT_SQL = 'SELECT {columns} FROM {table}'
    CREATE_TABLE = 'CREATE TABLE {table}'

    def __init__(self, user=None, password=None, host=None, database=None):
        self.user = user or PG_USER
        self.password = password or PG_PASSWORD
        self.host = host or PG_HOST
        self.database = database or PG_DATABASE
        self._conn = self.connect
        self._conn.autocommit = True
        self.cur = self.cursor

    @property
    def connect(self):
        return psycopg2.connect(host=self.host, dbname=self.database, user=self.user, password=self.password)

    @property
    def cursor(self):
        return self._conn.cursor(cursor_factory=RealDictCursor)

    def insert(self, table, **kwargs):
        columns = []
        values = []
        for column, value in kwargs.items():
            columns.append(column)
            values.append(value)
        if columns and values:
            columns = ','.join(columns)
            values = ','.join([str(value) if isinstance(value, int) else "'{}'".format(value) for value in values])
            insert_query = self.INSERT_SQL.format(table=table, columns=columns, values=values)
            logging.debug(insert_query)
            self.cur.execute(insert_query)

    def select(self, *columns, condidion='AND', table, group_by=None, **kwargs):
        assert condidion.lower() in ['or', 'and']
        sql_columns = ', '.join(columns) if columns else '*'
        result_query = self.SELECT_SQL.format(columns=sql_columns, table=table)
        if kwargs:
            conditions = f' {condidion} '.join([f'{column}=\'{value}\'' for column, value in kwargs.items()])
            result_query = f'{result_query} WHERE {conditions}'
        if group_by:
            result_query = f'{result_query} GROUP BY {group_by}'
        logging.debug(result_query)
        self.cur.execute(result_query)
        return self.cur.fetchall()

    def list_in(self, *columns, condidion='AND', table, **kwargs):
        assert condidion.lower() in ['or', 'and']
        assert not kwargs.values() or all([isinstance(value, (list, tuple, set)) for value in kwargs.values()])
        sql_columns = ', '.join(columns) if columns else '*'
        result_query = self.SELECT_SQL.format(columns=sql_columns, table=table)
        conditions = []
        for column, values in kwargs.items():
            values = ', '.join([f'\'{value}\'' for value in values])
            conditions.append(f'{column} IN ({values})')
        conditions = f' {condidion} '.join(conditions)
        result_query = f'{result_query} WHERE {conditions}'
        logging.debug(result_query)
        self.cur.execute(result_query)
        return self.cur.fetchall()

    def update(self, update_fields, table, condidion='AND', **kwargs):
        assert isinstance(update_fields, (tuple, dict))
        assert kwargs
        update_fields = ', '.join([f'"{column}"=\'{value}\'' for column, value in update_fields.items()])
        conditions = f' {condidion} '.join([f'{column}=\'{value}\'' for column, value in kwargs.items()])
        result_query = self.UPDATE_SQL.format(table=table, update_fields=update_fields, conditions=conditions)
        logging.debug(result_query)
        self.cur.execute(result_query)
