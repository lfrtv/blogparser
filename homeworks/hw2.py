import logging
import re
import uuid
from Stemmer import Stemmer
from pymystem3 import Mystem
from config import STOPWORDS, LOG_LEVEL
from db.db import PostgresDb

logging.basicConfig(level=LOG_LEVEL)


if __name__ == '__main__':
    db = PostgresDb()
    stemmer = Stemmer('russian')
    mystem = Mystem()
    stopwords = open(STOPWORDS).read().split('\n')
    for article in db.select(table='articles'):
        words = re.sub('\W|\d', ' ', article["content"]).split()
        for word in words:
            word = word.lower()
            if word not in stopwords:
                porter_word = stemmer.stemWord(word)
                mystem_word = mystem.lemmatize(word)[0]
                logging.debug(f'Porter stemmer word {porter_word}\nMyStem stemmer word {mystem_word}')
                db.insert(table='words_porter', id=uuid.uuid4(), term=porter_word, article_id=article['id'])
                db.insert(table='words_mystem', id=uuid.uuid4(), term=mystem_word, article_id=article['id'])


