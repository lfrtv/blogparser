import logging
from math import log10

import config
from config import LOG_LEVEL
from db.db import PostgresDb
from homeworks.hw3 import lemmatize

logging.basicConfig(level=LOG_LEVEL)
db = PostgresDb()


def get_calcalated_articles():
    n = 0
    article_data = dict()
    for article in db.select('id', 'title', 'url', table='articles'):
        count = db.select('COUNT(*)', table='words_mystem', article_id=article['id'])[0]['count']
        article_data[article['id']] = dict(article=article, count=count, calculate=0)
        n += count
    n /= config.ARTICLE_COUNT
    return n, article_data


def get_calculated_terms(query):
    assert query, 'Query can not be empty'
    terms = dict()
    query_words = lemmatize(query)
    for term in db.list_in(table='terms_list', term_text=query_words):
        terms[term['term_id']] = dict(term_text=term['term_text'], idf=get_term_idf_count(term['term_id']))
    return terms


def get_term_idf_count(term_id):
    count = db.select('COUNT(*)', table='article_term', term_id=term_id)[0]['count']
    return log10((config.ARTICLE_COUNT - count + 0.5) / (count + 0.5))


def get_term_occurrence(term, article_id):
    term_count = db.select('COUNT(*)', table='words_mystem', article_id=article_id, term=term)[0]['count']
    all_term_count = db.select('COUNT(*)', table='words_mystem', article_id=article_id)[0]['count']
    return term_count / all_term_count


def get_article_bm25(query):
    assert query, 'Query can not be empty'
    n, articles = get_calcalated_articles()
    terms = get_calculated_terms(query=query)
    calculated_article = dict()
    for article_id, article_data in articles.items():
        calculated_article[article_id] = 0
        for term_id, term_data in terms.items():
            term_text = term_data['term_text']
            d = term_data['idf'] * ((get_term_occurrence(term_text, article_id) * (config.K + 1)) / (
                    get_term_occurrence(term_text, article_id) + config.K * (
                        1 - config.B + config.B * (article_data['count'] / n))))
            if d >= 0:
                article_data['calculate'] += d
    return articles


if __name__ == '__main__':
    query = 'задавай вопросы Латвия'
    articles_bm25 = get_article_bm25(query=query)
    sorted_articles = sorted(articles_bm25.items(), key=lambda article: article[1]['calculate'], reverse=True)
    for article_id, article_data in sorted_articles[:10]:
        bm25 = article_data['calculate']
        url = article_data['article']['url']
        title = article_data['article']['title']
        if bm25:
            logging.info(f'TITLE: {title}, URL: {url}, BM25: {bm25}')
