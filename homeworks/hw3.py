import logging
import re
import uuid

from psycopg2._psycopg import IntegrityError
from pymystem3 import Mystem

from config import LOG_LEVEL
from db.db import PostgresDb, STOPWORDS

logging.basicConfig(level=LOG_LEVEL)
db = PostgresDb()


def create_inverted_index():
    for term in db.select(table='words_mystem'):
        try:
            term_id = uuid.uuid4()
            db.insert(table='terms_list', term_id=term_id, term_text=term['term'])
        except IntegrityError:
            term_id = db.select('term_id', table='terms_list', term_text=term['term'])[0]['term_id']
        try:
            db.insert(table='article_term', article_id=term['article_id'], term_id=term_id)
        except IntegrityError:
            pass


def lemmatize(query):
    mystem = Mystem()
    stopwords = open(STOPWORDS).read().split('\n')
    words = [word.lower() for word in re.sub('\W', ' ', query).split()]
    return [mystem.lemmatize(word)[0] for word in words if word not in stopwords]


def search_articles_url(query):
    assert query and isinstance(query, str)
    query_words = lemmatize(query)
    logging.debug(f'Lemmitized query words: {query_words}')
    terms_id = [term['term_id'] for term in db.list_in('term_id', table='terms_list', term_text=query_words)]
    result_articles_id = None
    for term_id in terms_id:
        articles_id = {article['article_id'] for article in
                       db.select('article_id', table='article_term', term_id=term_id)}
        result_articles_id = articles_id if result_articles_id is None else result_articles_id & articles_id
    return [article['url'] for article in
            db.list_in('url', table='articles', id=result_articles_id)] if result_articles_id else []


if __name__ == '__main__':
    query = 'задавай вопросы Латвия'
    article_url = search_articles_url(query)
    logging.info(f'Articles url: {article_url}')
