import logging
import uuid

import requests
from parsel import Selector

from config import *
from db.db import PostgresDb

logging.basicConfig(level=LOG_LEVEL)


def get_article_urls(page, url=None):
    url = url or BLOG_ADDRESS
    page_url = url + 'page/{}'.format(page)
    response = requests.get(url=page_url)
    page_as_text = response.content.decode('utf8')
    selector = Selector(text=page_as_text)
    article_urls = selector.xpath('//div[@class="post__title"]/a/@href').getall()
    return article_urls


def get_article(url):
    article = {}
    response = requests.get(url=url)
    page_as_text = response.content.decode('utf8')
    selector = Selector(text=page_as_text)
    article['title'] = selector.xpath('//h1[@class="post__title"]/text()').get()
    body_selector = selector.xpath('string((//div[@class="post__text clearfix"])[2])')
    article['content'] = body_selector.get().replace('(adsbygoogle = window.adsbygoogle || []).push({});', '')
    keywords = selector.xpath('//div[@class="post__meta__tags"]/a/text()').getall()
    article['keywords'] = ';'.join(keywords)
    article['url'] = url
    article['id'] = uuid.uuid4()
    return article


def get_page_count(url=None):
    url = url or BLOG_ADDRESS
    response = requests.get(url=url)
    page_as_text = response.content.decode('utf8')
    selector = Selector(text=page_as_text)
    pages = selector.xpath('//a[@class="page-numbers"]/text()').getall()[-1]
    return int(pages[-1]) if pages else 1


def get_articles(url=None):
    article_count = 0
    is_completed = False
    articles = []
    url = url or BLOG_ADDRESS
    page_count = get_page_count(url=url)
    for page in range(1, page_count):
        try:
            logging.debug('Parsing {} page'.format(page))
            article_urls = get_article_urls(page=page, url=url)
            for article_url in article_urls:
                try:
                    article = get_article(url=article_url)
                    logging.debug('Parsed article {}'.format(article))
                    articles.append(article)
                    article_count += 1
                    if article_count >= ARTICLE_COUNT:
                        is_completed = True
                        break
                except Exception as e:
                    logging.error(e, exc_info=True)
            if is_completed:
                break
        except Exception as e:
            logging.error(e, exc_info=True)
    return articles


if __name__ == '__main__':
    postgres = PostgresDb()
    postgres.insert(table='students', id=STUDENT_ID, name=NAME, surname=SURNAME, mygroup=GROUP)
    for article in get_articles():
        try:
            postgres.insert(table='articles', student_id=STUDENT_ID, **article)
            logging.debug('Success insert article "{}"'.format(article['title']))
        except Exception as e:
            logging.error(e, exc_info=True)

