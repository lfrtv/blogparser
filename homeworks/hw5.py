import logging
from math import sqrt
from config import LOG_LEVEL
from db.db import PostgresDb
from homeworks.hw3 import lemmatize
from homeworks.hw4 import get_idf


logging.basicConfig(level=LOG_LEVEL)
db = PostgresDb()


def calc_article_idf(article_id):
    article_terms = db.select('"tf-idf"', table='article_term', article_id=article_id)
    len = 0
    for num in article_terms:
        len += num['tf-idf'] ** 2
    return sqrt(len)


def get_calculated_articles(vector_len, terms_idf):
    articles = {}
    for term_text, term_data in terms_idf.items():
        for article in db.select(table='articles'):
            article_id = article['id']
            term_article_tf_idf = db.select('"tf-idf"', table='article_term', article_id=article_id,
                                            term_id=term_data['term_id'])
            if term_article_tf_idf:
                term_article_tf_idf = term_article_tf_idf[0]['tf-idf']
                cos = float(term_article_tf_idf) * float(term_data['idf'])
                if article_id not in articles:
                    articles[article_id] = dict(article=article, cos=cos, article_idf=calc_article_idf(article_id))
                else:
                    articles[article_id]['cos'] += cos
    for article_id, data in articles.items():
        if data['cos']:
            data['cos'] = data['cos'] / vector_len / data['article_idf']
    return articles


def get_terms_idf(query):
    assert query, 'Query can not be empty'
    vector_len = 0
    terms_idf = dict()
    query_words = lemmatize(query)
    for article_term in db.list_in(table='terms_list', term_text=query_words):
        idf = get_idf(article_term['term_id'])
        term_text = article_term['term_text']
        terms_idf[term_text] = dict(term_id=article_term['term_id'], idf=idf)
        vector_len += idf ** 2
    return sqrt(vector_len), terms_idf


if __name__ == '__main__':
    query = 'задавай вопросы Латвия'
    vector_len, terms_idf = get_terms_idf(query)
    articles = get_calculated_articles(vector_len, terms_idf)
    sorted_articles = sorted(articles.items(), key=lambda article: article[1]['cos'], reverse=True)
    for article_id, data in sorted_articles[:10]:
        cos = data['cos']
        url = data['article']['url']
        if cos:
            logging.info(f'URL: {url}, COS: {cos}')

