import logging
import numpy as np
from config import LOG_LEVEL
from db.db import PostgresDb
from homeworks.hw3 import lemmatize
from homeworks.hw4 import get_idf

logging.basicConfig(level=LOG_LEVEL)
db = PostgresDb()


def get_terms_as_arr():
    terms = db.select('term_id', 'term_text', table='terms_list')
    terms_arr = [list(term.values()) for term in terms]
    return np.array(terms_arr)


def get_articles_as_arr():
    articles = db.select('id', 'title', 'url', table='articles')
    articles_arr = [[article['id'], [article['title'], article['url']], 0] for article in articles]
    return np.array(articles_arr)


def get_tf_idf_matrix(articles_arr, terms_arr):
    articles_count = db.select('COUNT(*)', table='articles')[0]['count']
    terms_count = db.select('COUNT(*)', table='terms_list')[0]['count']
    matrix = np.zeros((terms_count, articles_count))
    article_terms = db.select(table='article_term')
    for article_term in article_terms:
        article_index = np.where(articles_arr[:, 0] == article_term['article_id'])[0][0]
        term_index = np.where(terms_arr[:, 0] == article_term['term_id'])[0][0]
        matrix[term_index][article_index] = article_term['tf-idf']
    return matrix


def get_calculated_articles(query, articles_arr, terms_arr, k=5):
    query_words = lemmatize(query)
    matrix = get_tf_idf_matrix(articles_arr, terms_arr)
    terms_count = db.select('COUNT(*)', table='terms_list')[0]['count']
    u, s, vh = np.linalg.svd(matrix)
    u_k = u[:, :k]
    s_k = np.zeros((k, k))
    for i in range(k):
        s_k[i][i] = 1 / s[i]
    v_k = np.transpose(vh[:k, :])
    z = np.zeros(terms_count)
    for term in terms_arr:
        if term[1] not in query_words:
            continue
        term_index = np.where(terms_arr == term[1])[0][0]
        z[term_index] = get_idf(term[0])
    z = np.dot(np.dot(z, u_k), s_k)
    for i, article in enumerate(articles_arr):
        article[2] = get_cos(v_k[i], z)
    return articles_arr


def get_cos(v1, v2):
    if len(v1) == len(v2):
        len_ = sum([v1 * v2 for v1, v2 in zip(v1, v2)])
        len_v1 = sum([v ** 2 for v in v1])
        len_v2 = sum([v ** 2 for v in v2])
        return len_ / ((len_v1 * len_v2)**0.5)


if __name__ == '__main__':
    query = 'задавай вопросы Латвия'
    articles_arr = get_articles_as_arr()
    terms_arr = get_terms_as_arr()
    articles = get_calculated_articles(query=query, articles_arr=articles_arr, terms_arr=terms_arr)
    sorted_articles = sorted(articles, key=lambda article: article[2], reverse=True)
    for data in sorted_articles[:10]:
        logging.info(f'TITLE: {data[1][0]}, URL: {data[1][1]}, COS: {data[2]}')
