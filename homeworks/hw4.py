import logging
from math import log10

from config import ARTICLE_COUNT, LOG_LEVEL
from db.db import PostgresDb

logging.basicConfig(level=LOG_LEVEL)
db = PostgresDb()


def get_idf(term_id):
    word_articles_count = db.select('count(*)', table='article_term', term_id=term_id)[0]['count']
    idf = log10(ARTICLE_COUNT / word_articles_count)
    return idf


def calc_tf_idf():
    for term in db.select(table='terms_list'):
        idf = get_idf(term_id=term['term_id'])
        for article_term in db.select('count(article_id)', 'article_id', table='words_mystem', term=term['term_text'],
                                      group_by='article_id'):
            article_words_count = db.select('count(article_id)', table='words_mystem',
                                            article_id=article_term['article_id'],
                                            group_by='article_id')
            tf = article_term['count'] / article_words_count[0]['count']
            tf_idf = tf * idf
            db.update(update_fields={'tf-idf': tf_idf}, table='article_term', article_id=article_term['article_id'],
                      term_id=term['term_id'])


if __name__ == '__main__':
    calc_tf_idf()
