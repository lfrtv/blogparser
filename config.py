# Config
import os


# DIR
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DATA_DIR = os.path.join(BASE_DIR, 'data')

# DATA
STOPWORDS = os.path.join(DATA_DIR, 'stop-list.txt')

# Blog
BLOG_ADDRESS = 'https://arborio.ru/travel/'
ARTICLE_COUNT = 40

# Postgres
PG_HOST = 'localhost'
PG_PORT = 5432
PG_USER = 'postgres'
PG_PASSWORD = ''
PG_DATABASE = 'blog'

# Other
STUDENT_ID = 100 + 2
NAME = 'Aidar'
SURNAME = 'Askhatov'
GROUP = '11-502'

LOG_LEVEL = 'INFO'

# BM25
K = 1.2
B = 0.75
